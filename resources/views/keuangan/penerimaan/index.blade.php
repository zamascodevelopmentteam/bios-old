@extends('layouts.side_nav')  
@section('content') 
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="row">
            <div class="col-xl-12">
                <div class="m-portlet m-portlet--mobile ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Data Penerimaan
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
                                        <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl m-dropdown__toggle">
                                            <i class="la la-plus m--hide"></i>
                                            <i class="la la-ellipsis-h m--font-brand"></i>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav">
                                                            <li class="m-nav__item">
                                                                <a href="{{url('data-keuangan/penerimaan/tambah')}}" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-share"></i>
                                                                    <span class="m-nav__link-text">
                                                                        Create Post
                                                                    </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Kode Akun</th>
                                <th>Saldo</th>
                                <th>Tanggal Update</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@stop

@section('scriptBlock')
<script type="text/javascript" src="{{asset('js/datatables.min.js')}}"></script>
<script>
        var table = $(".dataTables-data");
        var dataTable = table.DataTable({
        responsive:!0,
        "serverSide":true,
        "processing":true,
        "ajax":{
            url : "{{url('/data-keuangan/penerimaan')}}"
        },
        dom:"<'col-sm-6'i><'col-sm-6'f><'col-sm-12'<'table-responsive'tr>><'col-sm-6'l><'col-sm-6'p>",
        language:{
            paginate:{
                previous:"&laquo;",
                next:"&raquo;"
            },search:"_INPUT_",
            searchPlaceholder:"Search..."
        },
        "columns":[
            {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
            {"data":"tanggal","name":"tanggal","searchable":true,"orderable":true},
            {"data":"kode_akun","name":"kode_akun","searchable":true,"orderable":true},
            {"data":"saldo","name":"saldo","searchable":true,"orderable":true},
            {"data":"tanggalupdate","name":"tanggalupdate","searchable":true,"orderable":true},
            {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
        ],
        order:[[1,"asc"]]
    });

</script>
<!-- end:: Body -->
@stop