@extends('layouts.side_nav')  
@section('content') 
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                   Tambah Data Saldo
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            <form class="m-form m-form--fit m-form--label-align-right" action="{{url('data-keuangan/saldo/tambah')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Tanggal
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="date" name="tanggal">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Kode Jenis Rekening
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="kodejenisrekening">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Nama Bank
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="nama_bank">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Saldo
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="number" name="saldo">
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-7">
                                                <input type="submit" name="simpan" class="btn btn-accent m-btn m-btn--air m-btn--custom" value="Simpan">
                                                &nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane active" id="m_user_profile_tab_2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- end:: Body -->
@endsection