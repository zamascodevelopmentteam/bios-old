@extends('layouts.side_nav')  
@section('content') 
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                   Ubah Layanan Kesehatan
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            <form class="m-form m-form--fit m-form--label-align-right" action="{{url('data-layanan/layanan-kesehatan/ubah').'/'.$data->id}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Kode Satker
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="kode_satker" value="{{$data->kode_satker}}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Tahun
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="tahun" value="{{$data->tahun}}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Bulan
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="bulan" value="{{$data->bulan}}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Kelas
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="kelas" value="{{$data->kelas}}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Jumlah Pasien
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="jumlah_pasien" value="{{$data->jumlah_pasien}}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Jumlah Hari
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="jumlah_hari" value="{{$data->jumlah_hari}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-7">
                                                <input type="submit" name="simpan" class="btn btn-accent m-btn m-btn--air m-btn--custom" value="Simpan">
                                                &nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane active" id="m_user_profile_tab_2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- end:: Body -->
@endsection