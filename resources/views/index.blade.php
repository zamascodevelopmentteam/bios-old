@extends('layouts.side_nav')  
@section('content') 

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                   Welcome Dashboard
                </h3>
            </div>
        </div>
    </div>
    <div class="m-content">
        
    </div>
</div>
<!-- {{-- close tag untuk di layout --}} -->
</div>
@endsection
@section('scriptBlock')
<script type="text/javascript">
// var totals=[0,0,0,0,0,0,0,0,0];
// $(document).ready(function(){

//     var $dataRows=$("#omset_pme tr:not('.titlerow')");
    
//     $dataRows.each(function() {
//         $(this).find('.datanya').each(function(i){        
//             totals[i]+=parseInt( $(this).html());
//         });
//     });
//     $("#omset_pme td.totalnya").each(function(i){  
//         $(this).html(totals[i].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//         $('#omset_pme_atas td.target1').html(totals[1].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//         $('#omset_pme_atas td.target2').html(totals[3].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//         $('#omset_pme_atas td.totalnya1').html(totals[5].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//         $('#omset_pme_atas td.totalnya2').html(totals[7].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//     });
//     $("#omset_pme td.totalnya").each(function(i){  
//         $(this).html(totals[i].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//         $('#persen_pme td.target1').html(totals[1].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//         $('#persen_pme td.target2').html(totals[3].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//         $('#persen_pme td.totalnya1').html(totals[5].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//         $('#persen_pme td.totalnya2').html(totals[7].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//         totalomset = (totals[5] + totals[7]) / (totals[1] + totals[3]) * 100;
//         capaian1 = totals[5] / totals[1] * 100;
//         capaian2 = totals[7] / totals[3] * 100;
//         $('#persen_pme td.capaian1').html(capaian1.toFixed(2) + "%");
//         $('#persen_pme td.capaian2').html(capaian2.toFixed(2) + "%");
//         $('#persen_pme td.capaiantotal').html(totalomset.toFixed(2) + "%");
//     });

// });

// var total2=[0,0,0];
// $(document).ready(function(){

//     var $dataRows=$("#peserta_pme tr:not('.titlerowna')");
    
//     $dataRows.each(function() {
//         $(this).find('.colomngitung').each(function(i){        
//             total2[i]+=parseInt( $(this).html());
//         });
//     });
//     $("#peserta_pme td.colomntotal").each(function(i){  
//         $(this).html(total2[i]);
//     });

// });
</script>
@stop