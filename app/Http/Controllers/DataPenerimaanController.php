<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use DateTime;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\DataPenerimaan;
use Illuminate\Support\Facades\Redirect;

class DataPenerimaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = DataPenerimaan::select( DB::raw('*, data_penerimaan.id as Id, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'
                                <a href="penerimaan/ubah/'.$data->Id.'" style="float: left;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">
                                    <i class="la la-edit"></i>                        
                                </a> &nbsp;
                                <a href="penerimaan/hapus'.'/'.$data->Id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                    <i class="la la-trash"></i>                        
                                </a>'
                        ;})
            ->make(true);
        }
        return view('keuangan.penerimaan.index');
    }

    public function edit($id)
    {
        $data = DB::table('data_penerimaan')->where('id', $id)->first();
        // dd($data);
        return view('keuangan.penerimaan.update', compact('data'));
    }

    public function in()
    {
        return view('keuangan.penerimaan.insert');
    }

    public function insert(\Illuminate\Http\Request $request)
    {
        $now = new DateTime();
        $input = $request->all();

        $save = new DataPenerimaan;
        $save->tanggal = $input['tanggal'];
        $save->kode_akun = $input['kode_akun'];
        $save->saldo = $input['saldo'];
        $save->tanggalupdate = $now;
        $save->save();

        Session::flash('message', 'Data Penerimaan Keuangan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-keuangan/penerimaan');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {        
        $now = new DateTime();
        $ubah['tanggal'] = $request->tanggal;
        $ubah['kode_akun'] = $request->kode_akun;
        $ubah['saldo'] = $request->saldo;
        $ubah['tanggalupdate'] = $now;

        DataPenerimaan::where('id',$id)->update($ubah);
        Session::flash('message', 'Data Penerimaan Keuangan Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-keuangan/penerimaan');
    }    


    public function delete($id){
        Session::flash('message', 'Data Penerimaan Keuangan Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        DataPenerimaan::find($id)->delete();
        return redirect("data-keuangan/penerimaan");
    }
}
