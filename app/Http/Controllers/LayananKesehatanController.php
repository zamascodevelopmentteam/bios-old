<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use DateTime;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\LayananKesehatan;
use Illuminate\Support\Facades\Redirect;

class LayananKesehatanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = LayananKesehatan::select( DB::raw('*, layanan_kesehatan.id as Id, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'
                                <a href="layanan-kesehatan/ubah/'.$data->Id.'" style="float: left;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">
                                    <i class="la la-edit"></i>                        
                                </a> &nbsp;
                                <a href="layanan-kesehatan/hapus'.'/'.$data->Id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                    <i class="la la-trash"></i>                        
                                </a>'
                        ;})
            ->make(true);
        }
        return view('layanan.kesehatan.index');
    }

    public function edit($id)
    {
        $data = DB::table('layanan_kesehatan')->where('id', $id)->first();
        // dd($data);
        return view('layanan.kesehatan.update', compact('data'));
    }

    public function in()
    {
        return view('layanan.kesehatan.insert');
    }

    public function insert(\Illuminate\Http\Request $request)
    {
        $now = new DateTime();
        $input = $request->all();

        $save = new LayananKesehatan;
        $save->kode_satker = $input['kode_satker'];
        $save->tahun = $input['tahun'];
        $save->bulan = $input['bulan'];
        $save->kelas = $input['kelas'];
        $save->jumlah_pasien = $input['jumlah_pasien'];
        $save->jumlah_hari = $input['jumlah_hari'];
        $save->tgl_update = $now;
        $save->save();

        Session::flash('message', 'Data Layanan Kesehatan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-layanan/layanan-kesehatan');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $now = new DateTime();

        $ubah['kode_satker'] = $request->kode_satker;
        $ubah['tahun'] = $request->tahun;
        $ubah['bulan'] = $request->bulan;
        $ubah['kelas'] = $request->kelas;
        $ubah['jumlah_pasien'] = $request->jumlah_pasien;
        $ubah['jumlah_hari'] = $request->jumlah_hari;
        $ubah['tgl_update'] = $now;

        LayananKesehatan::where('id',$id)->update($ubah);
        Session::flash('message', 'Data Layanan Kesehatan Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-layanan/layanan-kesehatan');
    }    


    public function delete($id){
        Session::flash('message', 'Data Layanan Kesehatan Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        LayananKesehatan::find($id)->delete();
        return redirect("data-layanan/layanan-kesehatan");
    }
}
