<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use DateTime;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\DataPengeluaran;
use Illuminate\Support\Facades\Redirect;

class DataPengeluaranController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = DataPengeluaran::select( DB::raw('*, data_pengeluaran.id as Id, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'
                                <a href="pengeluaran/ubah/'.$data->Id.'" style="float: left;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">
                                    <i class="la la-edit"></i>                        
                                </a> &nbsp;
                                <a href="pengeluaran/hapus'.'/'.$data->Id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                    <i class="la la-trash"></i>                        
                                </a>'
                        ;})
            ->make(true);
        }
        return view('keuangan.pengeluaran.index');
    }


    public function edit($id)
    {
        $data = DB::table('data_pengeluaran')->where('id', $id)->first();
        // dd($data);
        return view('keuangan.pengeluaran.update', compact('data'));
    }

    public function in()
    {
        return view('keuangan.pengeluaran.insert');
    }

    public function insert(\Illuminate\Http\Request $request)
    {
        $now = new DateTime();
        $input = $request->all();

        $save = new DataPengeluaran;
        $save->tanggal = $input['tanggal'];
        $save->kode_akun = $input['kode_akun'];
        $save->saldo = $input['saldo'];
        $save->tanggalupdate = $now;
        $save->save();

        Session::flash('message', 'Data Pengeluaran Keuangan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-keuangan/pengeluaran');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $now = new DateTime();
        $ubah['tanggal'] = $request->tanggal;
        $ubah['kode_akun'] = $request->kode_akun;
        $ubah['saldo'] = $request->saldo;
        $ubah['tanggalupdate'] = $now;

        DataPengeluaran::where('id',$id)->update($ubah);
        Session::flash('message', 'Data Pengeluaran Keuangan Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-keuangan/pengeluaran');
    }    


    public function delete($id){
        Session::flash('message', 'Data Pengeluaran Keuangan Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        DataPengeluaran::find($id)->delete();
        return redirect("data-keuangan/pengeluaran");
    }
}
