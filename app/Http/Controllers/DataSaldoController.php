<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use DateTime;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\DataSaldo;
use Illuminate\Support\Facades\Redirect;

class DataSaldoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = DataSaldo::select( DB::raw('*, data_saldo.id as Id, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'
                                <a href="saldo/ubah/'.$data->Id.'" style="float: left;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">
                                    <i class="la la-edit"></i>                        
                                </a> &nbsp;
                                <a href="saldo/hapus'.'/'.$data->Id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                    <i class="la la-trash"></i>                        
                                </a>'
                        ;})
            ->make(true);
        }
        return view('keuangan.saldo.index');
    }


    public function edit($id)
    {
        $data = DB::table('data_saldo')->where('id', $id)->first();
        // dd($data);
        return view('keuangan.saldo.update', compact('data'));
    }

    public function in()
    {
        return view('keuangan.saldo.insert');
    }

    public function insert(\Illuminate\Http\Request $request)
    {
        $now = new DateTime();
        $input = $request->all();

        $save = new DataSaldo;
        $save->tanggal = $input['tanggal'];
        $save->kodejenisrekening = $input['kodejenisrekening'];
        $save->nama_bank = $input['nama_bank'];
        $save->saldo = $input['saldo'];
        $save->tanggalupdate = $now;
        $save->save();

        Session::flash('message', 'Data Saldo Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-keuangan/saldo');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $now = new DateTime();
        $ubah['tanggal'] = $request->tanggal;
        $ubah['kodejenisrekening'] = $request->kodejenisrekening;
        $ubah['nama_bank'] = $request->nama_bank;
        $ubah['saldo'] = $request->saldo;
        $ubah['tanggalupdate'] = $now;

        DataSaldo::where('id',$id)->update($ubah);
        Session::flash('message', 'Data Saldo Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-keuangan/saldo');
    }    


    public function delete($id){
        Session::flash('message', 'Data saldo Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        DataSaldo::find($id)->delete();
        return redirect("data-keuangan/saldo");
    }
}
