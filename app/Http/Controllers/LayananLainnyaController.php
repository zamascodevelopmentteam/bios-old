<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use DateTime;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\LayananLainnya;
use Illuminate\Support\Facades\Redirect;

class LayananLainnyaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = LayananLainnya::select( DB::raw('*, layanan_lainnya.id as Id, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'
                                <a href="layanan-lainnya/ubah/'.$data->Id.'" style="float: left;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">
                                    <i class="la la-edit"></i>                        
                                </a> &nbsp;
                                <a href="layanan-lainnya/hapus'.'/'.$data->Id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                    <i class="la la-trash"></i>                        
                                </a>'
                        ;})
            ->make(true);
        }
        return view('layanan.lainnya.index');
    }

    public function edit($id)
    {
        $data = DB::table('layanan_lainnya')->where('id', $id)->first();
        // dd($data);
        return view('layanan.lainnya.update', compact('data'));
    }

    public function in()
    {
        return view('layanan.lainnya.insert');
    }

    public function insert(\Illuminate\Http\Request $request)
    {
        $now = new DateTime();
        $input = $request->all();

        $save = new LayananLainnya;
        $save->kode_satker = $input['kode_satker'];
        $save->tahun = $input['tahun'];
        $save->bulan = $input['bulan'];
        $save->indikator = $input['indikator'];
        $save->jumlah = $input['jumlah'];
        $save->tgl_update = $now;
        $save->save();

        Session::flash('message', 'Data Layanan Lainnya Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-layanan/layanan-lainnya');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $now = new DateTime();

        $ubah['kode_satker'] = $request->kode_satker;
        $ubah['tahun'] = $request->tahun;
        $ubah['bulan'] = $request->bulan;
        $ubah['indikator'] = $request->indikator;
        $ubah['jumlah'] = $request->jumlah;
        $ubah['tgl_update'] = $now;

        LayananLainnya::where('id',$id)->update($ubah);
        Session::flash('message', 'Data Layanan Lainnya Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('data-layanan/layanan-lainnya');
    }    


    public function delete($id){
        Session::flash('message', 'Data Layanan Lainnya Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        LayananLainnya::find($id)->delete();
        return redirect("data-layanan/layanan-lainnya");
    }
}
