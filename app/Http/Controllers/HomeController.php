<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    public function dashboard()
    {
        // $tanggal = DB::connection('mysql4')
        //     ->table('tahun')
        //     ->first();

        // //------------omset pme sesuai bidang------------//
        // $pesertapme = DB::connection('mysql2')->table('badan_usaha')->get();
        // foreach ($pesertapme as $skey => $r)
        // {
        //     $checkSiklus1 = DB::connection('mysql2')->table('tb_registrasi')
        //         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
        //         ->where('perusahaan.pemerintah','=',$r->id)
        //         ->where('tb_registrasi.siklus','=',1)
        //         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal->tahun)
        //         ->count();
        //     $r->siklus1 = $checkSiklus1;
        //     $checkSiklus2 = DB::connection('mysql2')->table('tb_registrasi')
        //         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
        //         ->where('perusahaan.pemerintah','=',$r->id)
        //         ->where('tb_registrasi.siklus','=',2)
        //         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal->tahun)
        //         ->count();
        //     $r->siklus2 = $checkSiklus2;

        //     $r->jumlah = $checkSiklus1 + $checkSiklus2;
        // }

        // $fasyankes1 = DB::connection('mysql3')->table('badan_usaha')
        //     ->join('perusahaan','perusahaan.pemerintah' ,'badan_usaha.id')
        //     ->leftjoin('tb_registrasi', 'tb_registrasi.perusahaan_id', 'perusahaan.id')
        //     ->where('tb_registrasi.siklus','=',1)
        //     ->count();
        // $fasyankes2 = DB::connection('mysql3')->table('badan_usaha')
        //     ->join('perusahaan','perusahaan.pemerintah' ,'badan_usaha.id')
        //     ->leftjoin('tb_registrasi', 'tb_registrasi.perusahaan_id', 'perusahaan.id')
        //     ->where('tb_registrasi.siklus','=',2)
        //     ->count();
        // $fasyankestot = DB::connection('mysql3')->table('badan_usaha')
        //     ->join('perusahaan','perusahaan.pemerintah' ,'badan_usaha.id')
        //     ->leftjoin('tb_registrasi', 'tb_registrasi.perusahaan_id', 'perusahaan.id')
        //     ->where('tb_registrasi.siklus','=',2)
        //     ->orwhere('tb_registrasi.siklus','=',1)
        //     ->count();
        // //-----END-----//

        // //------------omset pme sesuai bidang------------//
        // $bidang = DB::table(DB::raw('pnpme.sub_bidang join pelayanan.master_target on pelayanan.master_target.bidang = pnpme.sub_bidang.id'))->where('pelayanan.master_target.tahun', $tanggal->tahun)->get();
        // foreach($bidang as $skey => $r)
        // {
        //     $checkSiklus1 = DB::connection('mysql2')->table('sub_bidang')
        //         ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
        //         ->select(DB::raw('sub_bidang.id, COUNT(tb_registrasi.id) AS Volume, SUM(IF(sub_bidang.id < 6, IF(tb_registrasi.siklus = 1, sub_bidang.tarif / 2, sub_bidang.tarif), IF(tb_registrasi.siklus = 1, sub_bidang.tarif, sub_bidang.tarif * 2))) as Total1'))
        //         ->wherein('tb_registrasi.siklus', array(1,12))
        //         ->where('sub_bidang.id','=', $r->id)
        //         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal->tahun)
        //         ->groupBy('sub_bidang.parameter')
        //         ->groupBy('sub_bidang.id')
        //         ->first();
        //     $r->chec1 = $checkSiklus1;
        //     if ($r->chec1 != NULL) {
        //         $r->total1 = $r->chec1->Total1;
        //         $r->Volume1 = $r->chec1->Volume;
        //         $r->Total1 = $r->chec1->Total1;
        //     }else{
        //         $r->total1 = '0';
        //         $r->Volume1 = '0';
        //         $r->Total1 = '0';
        //     }

        //     $checkSiklus2 = DB::connection('mysql2')->table('sub_bidang')
        //         ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
        //         ->select(DB::raw('sub_bidang.id, COUNT(tb_registrasi.id) AS Volume, SUM(IF(sub_bidang.id < 6, IF(tb_registrasi.siklus = 1, sub_bidang.tarif / 2, sub_bidang.tarif), IF(tb_registrasi.siklus = 1, sub_bidang.tarif, sub_bidang.tarif * 2))) as Total1'))
        //         ->wherein('tb_registrasi.siklus', array(2,12))
        //         ->where('sub_bidang.id','=', $r->id)
        //         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal->tahun)
        //         ->groupBy('sub_bidang.parameter')
        //         ->groupBy('sub_bidang.id')
        //         ->first();
        //     $r->chec2 = $checkSiklus2;

        //     if ($r->chec2 != NULL) {
        //         $r->total2 = $r->chec2->Total1;
        //         $r->Volume2 = $r->chec2->Volume;
        //         $r->Total2 = $r->chec2->Total1;
        //     }else{
        //         $r->total2 = '0';
        //         $r->Volume2 = '0';
        //         $r->Total2 = '0';
        //     }

        //     $r->pesivo1 = $r->Volume1 / $r->qty1 * 100;
        //     $r->pesivo2 = $r->Volume2 / $r->qty2 * 100;
        //     $r->pesiru1 = $r->Total1 / $r->nilai1 * 100;
        //     $r->pesiru2 = $r->Total2 / $r->nilai2 * 100;
        //     $r->jumlah = $r->total1 + $r->total2;
        // }
        // //-----END-----//

        // $data = DB::connection('mysql4')
        //     ->table('master_output')
        //     ->get();
        // foreach($data as $skey => $r)
        // {
        //     $transaksi = DB::connection('mysql4')
        //         ->table('master_transaksi')
        //         ->join('master_detail_akun', 'master_transaksi.id_detail_akun', '=', 'master_detail_akun.id')
        //         ->where('kode_output', $r->kode_output)
        //         ->where(DB::raw('YEAR(master_transaksi.created_at)'), '=' , $tanggal->tahun)
        //         ->select(DB::raw('SUM(nilai) as realisasi'))
        //         ->first();
        //     $r->transaksi = $transaksi;

        //     $totaloutput = DB::connection('mysql4')
        //         ->table('master_detail_akun')
        //         ->where('kode_output', $r->kode_output)
        //         ->select(DB::raw('SUM(jumlah) as total'))
        //         ->groupBy('kode_program')
        //         ->first();
        //     $r->total_output = $totaloutput;
        //     $r->saldo = $totaloutput->total - $transaksi->realisasi;
        //     $r->persentase = $transaksi->realisasi / $totaloutput->total * 100;
        // }

        // $totalprogram = DB::connection('mysql4')
        //     ->table('master_detail_akun')
        //     ->select(DB::raw('SUM(jumlah) as total'))
        //     ->groupBy('kode_program')
        //     ->first();
        // $transaksi = DB::connection('mysql4')
        //     ->table('master_transaksi')
        //     ->join('master_detail_akun', 'master_transaksi.id_detail_akun', '=', 'master_detail_akun.id')
        //     ->select(DB::raw('SUM(nilai) as realisasi'))
        //     ->where(DB::raw('YEAR(master_transaksi.created_at)'), '=' , $tanggal->tahun)
        //     ->first();
        // $saldo = $totalprogram->total - $transaksi->realisasi;
        // $persentase = $transaksi->realisasi / $totalprogram->total * 100;
        
        // $omsetHarian    = $this->omsetHarian(date('m-j'));
        // $omsetBulanan   = $this->omsetBulanan(date('m'));
        // $omsetTahunan   = $this->omsetTahunan($tanggal->tahun);
        // $omsetPelayanan = $this->omsetPelayanan($tanggal->tahun);
        
        // return view('index', compact('persentase', 'totalprogram', 'transaksi','saldo','data', 'bidang', 'pesertapme','tanggal', 'omsetHarian', 'omsetBulanan', 'omsetTahunan', 'omsetPelayanan', 'fasyankes1', 'fasyankes2', 'fasyankestot'));
        return view('index');
    }

    public function subQueryOmset()
    {
        // Dashboard pelayanan Omset Pelayanan
        $tanggal = DB::connection('mysql4')
            ->table('tahun')
            ->first();
        $subDatas = DB::connection('pgsql')
            ->table(DB::raw('tr_registrasi a'))
            ->select(
                DB::raw('EXTRACT(MONTH FROM a.tanggal_registrasi) as bulan'),
                DB::raw('EXTRACT(DAY FROM a.tanggal_registrasi) as hari'),
                DB::raw('EXTRACT(YEAR FROM a.tanggal_registrasi) as tahun'),
                'f.id',
                'f.nama',
                'c.biaya',
                'd.id_contoh'
            )
            ->leftJoin(DB::raw('tr_pembayaran b'), 'a.id', '=', 'b.id_registrasi')
            ->leftJoin(DB::raw('tr_pembayaran_item c'), 'b.id', '=', 'c.id_pembayaran')
            ->leftJoin(DB::raw('tr_pengujian d'), 'c.id_contoh', '=', 'd.id_contoh')
            ->leftJoin(DB::raw('mt_jenis_pengujian e'), 'd.id_jenis_pengujian', '=', 'e.id')
            ->leftJoin(DB::raw('mt_lab f'), 'e.id_lab', '=', 'f.id')
            // ->where('f.id', '=', DB::raw("$idLab"))
            ->where(DB::raw('EXTRACT(YEAR FROM a.tanggal_registrasi)'), '=',  DB::raw("'$tanggal->tahun'"))
            ->distinct()
            ->toSql();

        return $subDatas;
    }

    public function omsetHarian($hari)
    {
        $subDatas = $this->subQueryOmset();
        $datas = DB::connection('pgsql')
            ->table(DB::raw("($subDatas) Y"))
            ->select(
                DB::raw("CONCAT(bulan, '-', hari) AS harian"),
                DB::raw('sum(biaya) AS jumlah')
            )
            ->groupBy(DB::raw('harian'))
            ->havingRaw("CONCAT(bulan, '-', hari) = '$hari'")
            ->orderBy('harian')
            ->first();

        return $datas;
    }

    public function omsetBulanan($bulan)
    {
        $subDatas = $this->subQueryOmset();
        $datas = DB::connection('pgsql')
            ->table(DB::raw("($subDatas) Y"))
            ->select(
                'bulan',
                DB::raw('sum(biaya) AS jumlah')
            )
            ->groupBy(DB::raw('bulan'))
            ->havingRaw("bulan = '$bulan'")
            ->orderBy('bulan')
            ->first();

        return $datas;
    }

    public function omsetTahunan($tahun)
    {
        $subDatas = $this->subQueryOmset();
        $datas = DB::connection('pgsql')
            ->table(DB::raw("($subDatas) Y"))
            ->select(
                'tahun',
                DB::raw('sum(biaya) AS jumlah')
            )
            ->groupBy(DB::raw('tahun'))
            ->havingRaw("tahun = '$tahun'")
            ->orderBy('tahun')
            ->first();

        return $datas;
    }

    public function omsetPelayanan($tahun)
    {
        $subDatas = $this->subQueryOmset();
        $datas = DB::connection('pgsql')
            ->table(DB::raw("($subDatas) Y"))
            ->select(
                'tahun',
                DB::raw('coalesce(id, 212) AS id'),
                DB::raw("coalesce(nama, 'LAINNYA') AS nama"),
                DB::raw('sum(biaya) AS jumlah')
            )
            ->groupBy(DB::raw('tahun, nama, id'))
            ->havingRaw("tahun = '$tahun'")
            ->orderBy('tahun')
            ->get();

        return $datas;
    }
}
