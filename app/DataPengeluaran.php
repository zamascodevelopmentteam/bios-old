<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataPengeluaran extends Model{
    protected $table = 'data_pengeluaran';
    protected $fillable = ['tanggal','kode_kun','saldo']; 
}