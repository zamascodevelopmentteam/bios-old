<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataSaldo extends Model{
    protected $table = 'data_saldo';
    protected $fillable = ['tanggal','kodejenisrekening','nama_bank','saldo','tanggalupdate']; 
}