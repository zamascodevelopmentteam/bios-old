<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataPenerimaan extends Model{
    protected $table = 'data_penerimaan';
    protected $fillable = ['tanggal','kode_kun','saldo','tanggalupdate']; 
}