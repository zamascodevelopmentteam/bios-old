<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LayananKesehatan extends Model{
    protected $table = 'layanan_kesehatan';
    protected $fillable = ['kode_satker','tahun','bulan','kelas','jumlah_pasien','jumlah_hari','tgl_update']; 
}