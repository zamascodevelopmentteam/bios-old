<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LayananLainnya extends Model{
    protected $table = 'layanan_lainnya';
    protected $fillable = ['kode_satker','tahun','bulan','indikator','jumlah','tgl_update']; 
}