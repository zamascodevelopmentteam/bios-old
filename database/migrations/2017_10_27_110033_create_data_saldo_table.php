<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataSaldoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_saldo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tanggal');
            $table->string('kodejenisrekening');
            $table->string('nama_bank');
            $table->string('saldo');
            $table->dateTime('tanggalupdate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_saldo');
    }
}
