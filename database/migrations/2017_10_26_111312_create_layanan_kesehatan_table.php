<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayananKesehatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan_kesehatan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_satker');
            $table->string('tahun');
            $table->string('bulan');
            $table->string('kelas');
            $table->string('jumlah_pasien');
            $table->string('jumlah_hari');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layanan_kesehatan');
    }
}
