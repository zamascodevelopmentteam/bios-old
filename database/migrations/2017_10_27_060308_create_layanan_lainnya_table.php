<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayananLainnyaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan_lainnya', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_satker');
            $table->string('tahun');
            $table->string('bulan');
            $table->string('indikator');
            $table->string('jumlah');
            $table->dateTime('tgl_update');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layanan_lainnya');
    }
}
