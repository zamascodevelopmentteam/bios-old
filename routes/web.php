<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/home', function () {
    return redirect('index');
});

Route::get('/index', 'HomeController@dashboard');

Auth::routes();

// Route::get('/index', 'HomeController@index');

Route::get('data-layanan/layanan-kesehatan', 'LayananKesehatanController@index');
Route::get('data-layanan/layanan-kesehatan/tambah', 'LayananKesehatanController@in');
Route::get('data-layanan/layanan-kesehatan/ubah/{id}', 'LayananKesehatanController@edit');
Route::post('data-layanan/layanan-kesehatan/ubah/{id}', 'LayananKesehatanController@update');
Route::get('data-layanan/layanan-kesehatan/hapus/{id}', 'LayananKesehatanController@delete');
Route::post('data-layanan/layanan-kesehatan/tambah', 'LayananKesehatanController@insert');

Route::get('data-layanan/layanan-lainnya', 'LayananLainnyaController@index');
Route::get('data-layanan/layanan-lainnya/tambah', 'LayananLainnyaController@in');
Route::get('data-layanan/layanan-lainnya/ubah/{id}', 'LayananLainnyaController@edit');
Route::post('data-layanan/layanan-lainnya/ubah/{id}', 'LayananLainnyaController@update');
Route::get('data-layanan/layanan-lainnya/hapus/{id}', 'LayananLainnyaController@delete');
Route::post('data-layanan/layanan-lainnya/tambah', 'LayananLainnyaController@insert');

Route::get('/data-keuangan/penerimaan', 'DataPenerimaanController@index');
Route::get('/data-keuangan/penerimaan/tambah', 'DataPenerimaanController@in');
Route::get('/data-keuangan/penerimaan/ubah/{id}', 'DataPenerimaanController@edit');
Route::post('/data-keuangan/penerimaan/ubah/{id}', 'DataPenerimaanController@update');
Route::get('/data-keuangan/penerimaan/hapus/{id}', 'DataPenerimaanController@delete');
Route::post('/data-keuangan/penerimaan/tambah', 'DataPenerimaanController@insert');

Route::get('/data-keuangan/pengeluaran', 'DataPengeluaranController@index');
Route::get('/data-keuangan/pengeluaran/tambah', 'DataPengeluaranController@in');
Route::get('/data-keuangan/pengeluaran/ubah/{id}', 'DataPengeluaranController@edit');
Route::post('/data-keuangan/pengeluaran/ubah/{id}', 'DataPengeluaranController@update');
Route::get('/data-keuangan/pengeluaran/hapus/{id}', 'DataPengeluaranController@delete');
Route::post('/data-keuangan/pengeluaran/tambah', 'DataPengeluaranController@insert');

Route::get('/data-keuangan/saldo', 'DataSaldoController@index');
Route::get('/data-keuangan/saldo/tambah', 'DataSaldoController@in');
Route::get('/data-keuangan/saldo/ubah/{id}', 'DataSaldoController@edit');
Route::post('/data-keuangan/saldo/ubah/{id}', 'DataSaldoController@update');
Route::get('/data-keuangan/saldo/hapus/{id}', 'DataSaldoController@delete');
Route::post('/data-keuangan/saldo/tambah', 'DataSaldoController@insert');
